using System;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(EdgeCollider2D))]
public class RoadChunk: MonoBehaviour
{
    public Action<Vector2[]> roadChunkGenerated;
    [Range(2, 100)]
    [SerializeField] private int numberOfVertices = 10;
    [SerializeField] private float vertexStepLength = 5f;
    [SerializeField] private float maxLeftPos = 10f;
    [SerializeField] private float maxRightPos = -10f;    

    private EdgeCollider2D edgeCollider;
    private void Awake()
    {
        edgeCollider = GetComponent<EdgeCollider2D>();       
    }
    public void GetMaxLeftPos(float value)
    {
        maxLeftPos = value;
    }
    public void GetMaxRightPos(float value)
    {
        maxRightPos = value;
    }
    public void Generate()
    {  
        float shift = numberOfVertices * vertexStepLength / 2f;
        Generate(new Vector2(UnityEngine.Random.Range(maxRightPos, maxLeftPos), -shift));
    }
    public void Generate(Vector2 startPoint)
    {        
        Vector2[] points = new Vector2[numberOfVertices];        
        for (int i = 0; i < numberOfVertices; i++)
        {
            if (i == 0)
                points[i] = startPoint;
            else
                points[i] = new Vector2(UnityEngine.Random.Range(maxRightPos, maxLeftPos), vertexStepLength * i + startPoint.y);
        }
        edgeCollider.points = points;

        roadChunkGenerated?.Invoke(points);
    }

    public Vector2 GetFirstPoint()
    {
        return edgeCollider.points.First();        
    }
    public Vector2 GetLastPoint()
    {
        return edgeCollider.points.Last();
    }
    public Vector2 GetCenter()
    {
        return new Vector2(0f, (GetLastPoint().y - GetFirstPoint().y) / 2 + GetFirstPoint().y);
    }
}
