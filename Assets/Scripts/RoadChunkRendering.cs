using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class RoadChunkRendering : MonoBehaviour
{
    [SerializeField] private RoadChunk roadChank;

    private LineRenderer lineRenderer;
    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }
    private void OnEnable()
    {
        roadChank.roadChunkGenerated += RoadChunkGenerated;
    }
    private void OnDisable()
    {
        roadChank.roadChunkGenerated -= RoadChunkGenerated;
    }
    private void RoadChunkGenerated(Vector2[] vectors)
    {
        Vector3[] vectors3 = System.Array.ConvertAll<Vector2, Vector3>(vectors, getV3fromV2);
        lineRenderer.positionCount = vectors3.Length;
        lineRenderer.SetPositions(vectors3);
    }
    public Vector3 getV3fromV2(Vector2 v2)
    {

        return new Vector3(v2.x, v2.y, 0f);
    }
}
