using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoadBoxGenerator : MonoBehaviour
{
    [SerializeField] private GameObject RedBox;
    [SerializeField] private GameObject GreenBox;
    [Header("Offset for create box")]
    [SerializeField] private Vector3 offset;

    private Pool<Box> redPool;
    private Pool<Box> greenPool;
    private Coroutine CreateBoxCoroutine;
    private Coroutine DelOldBoxCoroutine;    

    private void Start()
    {
        redPool = new Pool<Box>(Instantiate, transform.parent.parent, RedBox);
        greenPool = new Pool<Box>(Instantiate, transform.parent.parent, GreenBox);
    }
    
    public void StartGenerator()
    {
        StopAllCoroutines();
        CreateBox();
        ClearOldBox();    
    }

    public void StopGenerator()
    {
        redPool.ClearAll();
        greenPool.ClearAll();
        if(CreateBoxCoroutine!=null)
            StopCoroutine(CreateBoxCoroutine);
        if(DelOldBoxCoroutine!=null)
            StopCoroutine(DelOldBoxCoroutine);
    }
    private void ClearOldBox()
    {       
        float yPosForDel = 7f;
        for (int i = redPool.GetActiveObjects().Count-1;i>=0 ; i--)
        {
            var box = redPool.GetActiveObjects()[i];       
            if (box.transform.position.y < transform.parent.position.y - yPosForDel)
                redPool.Set(box);
        }
        for (int i = greenPool.GetActiveObjects().Count - 1; i >= 0; i--)
        {
            var box = greenPool.GetActiveObjects()[i];
            if (box.transform.position.y < transform.parent.position.y - yPosForDel)
                greenPool.Set(box);
        }
        CreateBoxCoroutine = StartCoroutine(TimerAction(1, ClearOldBox));
    }
    private void CreateBox()
    {
        Box box;
        if (UnityEngine.Random.Range(0, 2) == 0)
        {
            box = redPool.Get();
            box.Init(redPool);
        }
        else
        {
            box = greenPool.Get();
            box.Init(greenPool);
        }

        box.transform.position = new Vector3(UnityEngine.Random.Range(-3f, 3f), transform.position.y) + offset;
        CreateBoxCoroutine = StartCoroutine(TimerAction(UnityEngine.Random.Range(1f, 2f), CreateBox));
    }
    private IEnumerator TimerAction(float seconds, Action finishAction)
    {
        yield return new WaitForSeconds(seconds);
        finishAction?.Invoke();
    }
}
public enum TypeItem 
{
    red = 1,
    green = 2,
}

