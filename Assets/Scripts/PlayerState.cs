using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerState : MonoBehaviour
{
    [SerializeField] private Player player;
    [SerializeField] private RoadBoxGenerator roadBoxGenerator;
    [SerializeField] private GameObject StartMenu;
    [SerializeField] private TextMeshProUGUI textPoints;
    [SerializeField] private TextMeshProUGUI textLives;    
    private void OnEnable()
    {
        player.GotHitAction += GotHitAction;
        player.GotPointAction += GotPointAction;
    }
    private void OnDisable()
    {
        player.GotHitAction -= GotHitAction;
        player.GotPointAction -= GotPointAction;
    }
    private void GotHitAction(int hits)
    {
        if (player.LivesCount >= hits)
        {
            textLives.text = "Кол-во жизней: " + (player.LivesCount - hits);
        }
        if (player.LivesCount == hits)
        {
            player.Stop();
            roadBoxGenerator.StopGenerator();
            StartMenu.SetActive(true);
        }
    } 
    private void GotPointAction(int points)
    {
        textPoints.text = "Кол-во очков: " + points;
    }
}
