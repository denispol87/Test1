using System;
using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Смена управления")]
    public bool ChangeDirectionByClick = true;

    [SerializeField] private float speedMove = 1f;
    [SerializeField] private float speedBoost = 1f;

    public float SpeedMove
    {
        get { return speedMove * speedBoost; }
        set { speedMove = value; }
    }
    public int LivesCount = 3;
    public Action<int> GotHitAction;
    public Action<int> GotPointAction;

    private Camera cam;
    private float moveHorizontal = 0f;
    private float moveVertical = 1f;
    private int hits = 0;
    private int points = 0;
    private Coroutine changeSpeedCoroutine;
    void Start()
    {
        cam = Camera.main;
        Restart();
    }
    public void Restart()
    {
        var pos = transform.position;
        pos.x = 0;
        transform.position = pos;
        moveHorizontal = 0f;
        moveVertical = 1f;
        hits = 0;
        points = 0;
        GotHitAction?.Invoke(hits);
        GotPointAction?.Invoke(points);
    }
    public void Stop()
    {
        moveVertical = 0f;
    }
    /// <summary>
    /// Смена направления движения по горизонтали по клику мышы
    /// </summary>
    private void MouseClickChangeMove()
    {
        if (moveHorizontal == 0)
            moveHorizontal = 1;
        else
            moveHorizontal *= -1;
    }
    /// <summary>
    /// Смена направления движения в сторону курсора
    /// </summary>
    private void MouseMovement()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = 10f;
        Vector3 mouseScreenPos = cam.ScreenToWorldPoint(mousePosition, Camera.MonoOrStereoscopicEye.Mono);
        Vector3 pos = transform.position;
        float distance = Mathf.Abs(mouseScreenPos.x - pos.x);        
        if (distance < 0.5f)
            moveHorizontal = 0f;
        else
        {
            if (mouseScreenPos.x < pos.x)
                moveHorizontal = -1f;
            else if (mouseScreenPos.x > pos.x)
                moveHorizontal = 1f;
        }
    }
    private void Update()
    {
        if (ChangeDirectionByClick)
        {
            if (Input.GetMouseButtonDown(0))
            {
                MouseClickChangeMove();
            }
        }
    }
    private void FixedUpdate()
    {
        if (!ChangeDirectionByClick)
        {
            MouseMovement();
        }
        MovementLogic();
    }
    
    private void MovementLogic()
    {        
        Vector3 movement = new Vector3(moveHorizontal, moveVertical, 0.0f);        
        transform.Translate(movement * SpeedMove * Time.fixedDeltaTime);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Road":
                GotHit();
                break;
            case "Item":
                Box item = collision.gameObject.GetComponent<Box>();
                if (item.TypeItem == TypeItem.red)
                    GotHit();
                else
                    GotPoint();
                break;
        }
    }        
    private void GotHit()
    {
        hits++;        
        GotHitAction?.Invoke(hits);
    }
    private void GotPoint()
    {
        points++;
        GotPointAction?.Invoke(points);
        CheckPoint();
    }
    private void CheckPoint()
    {
        /*
        10 - x1.5 (скорость); 
        25 - x2 (скорость); 
        50 - x3 (скорость); 
        100 - x4 (скорость).
        */
        switch (points)
        {
            case 10:
                ChangeSpeedBoost(1.5f);
                break;
            case 25:
                ChangeSpeedBoost(2f);
                break;
            case 50:
                ChangeSpeedBoost(3f);
                break;
            case 100:
                ChangeSpeedBoost(4f);
                break;

        }
    }
    private void ChangeSpeedBoost(float newSpeedBoost)
    {
        if (changeSpeedCoroutine != null)
            StopCoroutine(changeSpeedCoroutine);

        float smoothness = 10f;
        float speedChangeTime = 2f;// в секундах
        float startSpeedBoost = speedBoost;
        changeSpeedCoroutine = StartCoroutine(TimerAction(speedChangeTime, smoothness, (value) => {            
            speedBoost = Mathf.Lerp(startSpeedBoost, newSpeedBoost, value); 
        }));
    }

    private IEnumerator TimerAction(float seconds, float smoothness, Action<float> lerpStepAction)
    {
        float oneStep = seconds / smoothness;
        float lerpStep = 1f / smoothness;
        float lerp = 0f;
        while (lerp < 1f)
        {
            yield return new WaitForSeconds(oneStep);
            lerpStepAction?.Invoke(lerp);
            lerp += lerpStep;
        }
        lerpStepAction?.Invoke(1);
    }
    
}
