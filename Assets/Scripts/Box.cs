using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    public TypeItem TypeItem;
    private ISetObject<Box> setObjectInPool;    
    
    public void Init(ISetObject<Box> setObjectInPool)
    {
        this.setObjectInPool = setObjectInPool;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag!= "Road")
        {
            setObjectInPool.Set(this);
        }
    }
}
