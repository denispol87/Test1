using System.Linq;
using UnityEngine;

public class BuildingBorder : MonoBehaviour
{
    [SerializeField] private GameObject roadChank;
    [SerializeField] private int CountChank = 2;
    [SerializeField] private float maxLeftPos = 0f;
    [SerializeField] private float maxRightPos = -6f;

    private Pool<RoadChunk> poolRoadChunk;
    private RoadChunk LastRoadChank;
    private Transform cameraTransform;
    private void Start()
    {
        poolRoadChunk = new Pool<RoadChunk>(Instantiate, transform, roadChank);
        cameraTransform = Camera.main.transform;
        
        for (int i = 0; i < CountChank; i++)
        {
            RoadChunk chank = poolRoadChunk.Get();
            chank.GetMaxLeftPos(maxLeftPos);
            chank.GetMaxRightPos(maxRightPos);
            if (i == 0)
                chank.Generate();
            else
                chank.Generate(poolRoadChunk.GetActiveObjects()[i - 1].GetLastPoint());
        }
        LastRoadChank = poolRoadChunk.GetActiveObjects().Last();
    }    
    private void FixedUpdate()
    {
        if (poolRoadChunk.GetActiveObjects().Count == CountChank && cameraTransform.position.y > poolRoadChunk.GetActiveObjects().Last().GetCenter().y)
        { 
            poolRoadChunk.DelFirst();
            RoadChunk chank = poolRoadChunk.Get();
            chank.Generate(LastRoadChank.GetLastPoint());
            LastRoadChank = chank;
        }
    }
}
