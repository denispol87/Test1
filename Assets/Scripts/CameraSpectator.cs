using UnityEngine;

public class CameraSpectator : MonoBehaviour
{        
    [SerializeField] private Transform target; 
    [SerializeField] private Vector3 offset; 
    [SerializeField] private bool freezeX = true; 
        

    private void FixedUpdate()
    {
        if(target)
        {
            var newPos = target.position + offset;
            if (freezeX)
                newPos.x = 0f;
            transform.position = newPos;
        }
    }
}
